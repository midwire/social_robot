# SocialRobot

We want to be able to do things like this:

```ruby
require 'social_robot'

user = SocialRobot::User.deserialize('crazyhorse')
SocialRobot::Processors::Facebook.with_user(user) do |fb|
  fb.login
  fb.post("This is my story, and I'm sticking to it.")
  fb.like('https://www.facebook.com/?__fns&hash=Ac3spVmgYrbx5frd#')
  fb.logout
end

SocialRobot::Processors::Twitter.with_user(user) do |twitter|
  twitter.login
  twitter.tweet("This is my story, and I'm sticking to it.")
  twitter.favorite('https://twitter.com/JimGaffigan/status/694244864666550272')
  twitter.logout
end
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'social_robot'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install social_robot

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/social_robot.
