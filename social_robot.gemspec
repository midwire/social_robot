# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'social_robot/version'

Gem::Specification.new do |spec|
  spec.name          = 'social_robot'
  spec.version       = SocialRobot::VERSION
  spec.authors       = ['Chris Blackburn']
  spec.email         = ['87a1779b@opayq.com']

  spec.summary       = 'Automate your friends.'
  spec.description   = spec.summary
  spec.homepage      = 'https://bitbucket.org/midwire/social_robot'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3'
  spec.add_development_dependency 'simplecov', '~> 0.11'
  spec.add_development_dependency 'pry-nav'
  spec.add_development_dependency 'dotenv'

  spec.add_dependency 'midwire_common', '~> 0.1'
  spec.add_dependency 'capybara', '~> 2.6'
  spec.add_dependency 'poltergeist', '~> 1.9'
end
