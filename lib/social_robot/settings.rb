module SocialRobot
  class Settings < MidwireCommon::YamlSetting
    def initialize(config = SocialRobot.config_file)
      super(config)
    end
  end
end
