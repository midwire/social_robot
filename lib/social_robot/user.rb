module SocialRobot
  # A user model for social websites
  class User
    attr_reader :username
    attr_reader :password
    attr_reader :email
    attr_reader :proxy_host
    attr_reader :proxy_port
    attr_reader :proxy_user
    attr_reader :proxy_pass

    class << self
      def deserialize(username)
        YAML.load(File.read(data_file(username)))
      end

      def data_directory
        @data_dir ||= File.join(
          SocialRobot.home_directory,
          SocialRobot.default_config_directory,
          'data'
        )
      end

      def cookie_directory
        @cookie_dir ||= File.join(
          SocialRobot.home_directory,
          SocialRobot.default_config_directory,
          'cookies'
        )
      end

      def data_file(uname)
        File.join(data_directory, "#{uname}.yml")
      end
    end

    def initialize(options = {})
      @username   = options.fetch(:username) { fail('Username is required.') }
      @password   = options.fetch(:password) { fail('Password is required.') }
      @email      = options.fetch(:email, nil)
      @proxy_host = options.fetch(:proxy_host, nil)
      @proxy_port = options.fetch(:proxy_port, nil)
      @proxy_user = options.fetch(:proxy_user, nil)
      @proxy_pass = options.fetch(:proxy_pass, nil)
    end

    def serialize
      data = YAML.dump(self)
      FileUtils.mkdir_p(self.class.data_directory)
      File.open(self.class.data_file(username), 'w') { |f| f.write(data) }
    end

    def cookie_file
      File.join(self.class.cookie_directory, "#{username}.yml")
    end
  end
end
