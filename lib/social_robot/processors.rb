module SocialRobot
  module Processors
    autoload :Base,   'social_robot/processors/base'
    autoload :Reddit, 'social_robot/processors/reddit'
  end
end
