module SocialRobot
  module Processors
    # Base class for all SocialRobot processors.
    class Base
      DEFAULT_MIN_SLEEP = ENV.fetch('DEFAULT_MIN_SLEEP', 2).to_i

      attr_reader :user, :agent, :session

      def self.with_user(user)
        me = new(user: user)
        yield(me)
      end

      def initialize(options = {})
        @user    = options.fetch(:user) { fail('Must provide a user') }
        @agent   = options.fetch(:agent, Agent.new)
        @session = options.fetch(:session, {})
        load_cookies
      end

      protected

      def store_cookies
        agent.store_cookies(user.cookie_file)
      end

      def load_cookies
        agent.load_cookies(user.cookie_file)
      end

      def sleep_default
        sleep(DEFAULT_MIN_SLEEP)
      end

      def browser
        agent.browser
      end
    end
  end
end
