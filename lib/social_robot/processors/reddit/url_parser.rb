module SocialRobot
  module Processors
    module Reddit
      class UrlParser
        REGEX = %r{^https?://.*reddit\.com(/r/[a-z0-9]+)/comments/([a-z0-9]+)/([^/]+)/?(.+)?$}i

        attr_reader :subreddit
        attr_reader :original_post_id
        attr_reader :original_post_stub
        attr_reader :comment_id

        def initialize(url)
          md = url.match(REGEX)
          fail("Not a valid reddit URL: [#{url}]") unless md
          @subreddit = md[1]
          @original_post_id = md[2]
          @original_post_stub = md[3]
          @comment_id = md[4]
        end

        def op_id
          original_post_id
        end

        def op_stub
          original_post_stub
        end

        def original_post?
          comment_id.nil?
        end
      end
    end
  end
end
