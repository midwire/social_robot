module SocialRobot
  module Processors
    module Reddit
      autoload :UrlParser, 'social_robot/processors/reddit/url_parser'

      class Handler < Base
        BASE_URI = 'https://www.reddit.com'.freeze
        LOGIN_PAGE = "#{BASE_URI}/login".freeze

        def login
          return true if logged_in?
          agent.visit(LOGIN_PAGE)
          sleep_default
          write_login_session
          return true if logged_in?
          unless login_form
            # save_current_page
            fail("Error during login: Cannot find form: '#login-form'")
          end
          submit_login_form
          write_login_session
          if logged_in?
            store_cookies
            return true
          end
          false
        end

        def subscribe(subreddit)
          fail "This is not valid a subreddit: #{subreddit}" unless valid_subreddit?(subreddit)
          login_and_visit(subreddit_url(subreddit))
          return true if subscribed?
          browser.click_link('subscribe')
          subscribed?
        end

        def unsubscribe(subreddit)
          fail "This is not valid a subreddit: #{subreddit}" unless valid_subreddit?(subreddit)
          login_and_visit(subreddit_url(subreddit))
          return true unless subscribed?
          browser.click_link('unsubscribe')
          !subscribed?
        end

        # Returns +1 if voted up, -1 if unvoted up, 0 if unchanged
        def toggle_upvote_comment(link)
          toggle_vote(link, :up)
        end

        # Returns +1 if voted down, -1 if unvoted down, 0 if unchanged
        def toggle_downvote_comment(link)
          toggle_vote(link, :down)
        end

        private

        def toggle_vote(link, op)
          login_and_visit(link)
          top_selector = top_selector_for(link)
          bottom_selector = op == :up ? '.up' : '.down'
          node = browser.first("#{top_selector} .midcol #{bottom_selector}")
          value = 1
          if node.nil?
            bottom_selector = op == :up ? '.upmod' : '.downmod'
            node = browser.first("#{top_selector} .midcol #{bottom_selector}")
            value = -1
          end
          result = node.click if node
          result && result.key?('position') ? value : 0
        end

        def login_and_visit(link)
          login unless logged_in?
          agent.visit(link)
          sleep_default
        end

        def logged_in?
          session[:logged_in]
        end

        def login_form
          @login_form ||= agent.elements('#login-form').first
        end

        def submit_login_form
          login_form.fill_in('username', with: user.username)
          login_form.fill_in('password', with: user.password)
          login_form.check('rem')
          login_form.click_button('log in')
        end

        def valid_subreddit?(subreddit)
          !!subreddit.match(%r{(/r/[a-z0-9]+)}i)
        end

        def subreddit_url(subreddit)
          "#{BASE_URI}#{subreddit}"
        end

        def subscribed?
          body = agent.elements('body')
          return nil if body.empty?
          body.first.has_link?('unsubscribe')
        end

        def write_login_session
          body = agent.elements('body')
          return false if body.empty?
          session[:logged_in] = body.first.has_link?('logout')
        end

        def save_current_page(should_open = true)
          File.open('temp.html', 'w') { |f| f.write(agent.body) }
          system('open temp.html') if should_open
        end

        def top_selector_for(link)
          parsed = UrlParser.new(link)
          parsed.original_post? ? '#siteTable' : '.sitetable'
        end
      end
    end
  end
end
