require 'social_robot/version'
require 'social_robot/processors'

require 'midwire_common/string'
require 'midwire_common/yaml_setting'
require 'midwire_common/hash'

module SocialRobot
  class << self
    def root
      Pathname.new(File.dirname(__FILE__)).parent
    end

    def current_environment
      ENV.fetch('APP_ENV', 'development')
    end

    def string_id
      to_s.snakerize
    end

    def default_config_directory
      ".#{string_id}"
    end

    def default_config_filename
      ".#{string_id}.yml"
    end

    def home_directory
      ENV.fetch('HOME')
    end

    # If you don't have a HOME directory defined, you are on an OS that is
    # retarded, and this call will fail.
    def config_file
      dir = File.join(home_directory, default_config_directory)
      module_string = string_id.upcase
      file = ENV.fetch(
        "#{module_string}_CONFIG",
        File.join(dir, default_config_filename)
      )
      FileUtils.mkdir_p(dir)
      FileUtils.touch(file)
      file
    end

    def settings(config = config_file)
      @settings ||= Settings.new(config)
    end

    def logger
      @logger ||= Logger.instance
    end
  end

  autoload :Agent,    'social_robot/agent'
  autoload :Settings, 'social_robot/settings'
  autoload :User,     'social_robot/user'
end
