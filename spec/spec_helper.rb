$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
ENV['APP_ENV'] = 'test'

if ENV['COVERAGE']
  require 'simplecov'

  SimpleCov.start do
    add_filter '/bin'
    add_filter '/stubs'
    add_filter '/tmp'
    add_filter '/vendor'
    add_filter '/spec'

    add_group 'Modules', 'module'
    add_group 'Libraries', 'lib'
  end
end

require 'pry-nav'

require 'dotenv'
paths = %W(.env .env.#{ENV.fetch('APP_ENV', 'development')}).map do |name|
  Dir.pwd + '/' + name
end
Dotenv.load(*paths).each { |k, v| ENV[k] = v }

require 'social_robot'

Dir[SocialRobot.root.join('spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  config.mock_with :rspec
  config.color = true
  config.order = 'random'
  config.profile_examples = 3

  config.filter_run :focus
  config.run_all_when_everything_filtered = true

  config.disable_monkey_patching!
end
