require 'spec_helper'

RSpec.describe SocialRobot, type: :module do
  context '#root' do
    it 'returns a Pathname instance' do
      expect(described_class.root).to be_a(Pathname)
    end

    it 'returns the root directory' do
      dir = described_class.root
      expect(dir.to_s).to match(/#{described_class.string_id}$/)
      expect(File.exist?(dir)).to eq(true)
    end
  end

  context '#string_id' do
    it 'returns lowercase string name of the module' do
      expect(described_class.string_id).to eq(described_class.to_s.snakerize)
    end
  end

  context '#current_environment' do
    it "defaults to 'development'" do
      ENV.delete('APP_ENV')
      expect(described_class.current_environment).to eq('development')
    end

    it 'returns the APP_ENV variable' do
      ENV['APP_ENV'] = 'asdf'
      expect(described_class.current_environment).to eq('asdf')
    end
  end

  context '#config_file' do
    it 'returns the default config file' do
      expect(described_class.config_file).to match(/.#{described_class.string_id}.yml$/)
    end

    it 'ensures the file exists' do
      expect(File.exist?(described_class.config_file)).to eq(true)
    end
  end

  context '#settings' do
    it 'returns the YamlSetting instance' do
      expect(described_class.settings).to be_a(MidwireCommon::YamlSetting)
    end
  end
end
