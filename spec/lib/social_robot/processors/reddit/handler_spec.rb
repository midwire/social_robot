require 'spec_helper'

module SocialRobot
  module Processors
    module Reddit
      RSpec.describe Handler, type: :processor do
        let(:user_args) do
          {
            username:   ENV['REDDIT_USERNAME'],
            password:   ENV['REDDIT_PASSWORD'],
            email:      ENV['REDDIT_EMAIL'],
            proxy_host: ENV['REDDIT_PROXY_HOST'],
            proxy_port: ENV['REDDIT_PROXY_PORT'],
            proxy_user: ENV['REDDIT_PROXY_USER'],
            proxy_pass: ENV['REDDIT_PROXY_PASS']
          }
        end
        let(:user) { User.new(user_args) }
        let(:reddit) { described_class.new(user: user) }
        let(:subreddit_url) { "#{described_class::BASE_URI}/r/funny" }
        let(:original_post_url) { "#{subreddit_url}/comments/469n5e/how_my_brain_works" }
        let(:post_response) { "#{original_post_url}/d03kyhw" }

        context '#new' do
          it 'instantiates a Reddit processor' do
            expect(reddit).to be_a(described_class)
          end
        end

        context '.with_user' do
          it 'yields the correct processor' do
            described_class.with_user(user) do |x|
              expect(x).to be_a(described_class)
            end
          end
        end

        context '.login' do
          it 'logs the user into reddit' do
            expect(reddit.login).to eq(true)
          end
        end

        context '.subscribe' do
          it 'subscribes to a subreddit' do
            expect(reddit.subscribe('/r/bogus')).to eq(true)
          end
        end

        context '.unsubscribe' do
          it 'unsubscribe from a subreddit' do
            expect(reddit.unsubscribe('/r/bogus')).to eq(true)
          end
        end

        context '.toggle_upvote_comment' do
          context 'for original post' do
            it 'toggles a comment up vote' do
              expect(reddit.toggle_upvote_comment(original_post_url)).to be_truthy
            end
          end

          context 'for a post response' do
            it 'toggles a comment comment up vote' do
              expect(reddit.toggle_upvote_comment(post_response)).to be_truthy
            end
          end
        end

        context '.toggle_downvote_comment' do
          context 'for original post' do
            it 'toggles a comment down vote' do
              expect(reddit.toggle_downvote_comment(original_post_url)).to be_truthy
            end
          end

          context 'for a post response' do
            it 'toggles a comment comment down vote' do
              expect(reddit.toggle_downvote_comment(post_response)).to be_truthy
            end
          end
        end
      end
    end
  end
end
