require 'social_robot/processors/reddit/url_parser'

module SocialRobot
  module Processors
    module Reddit
      RSpec.describe UrlParser do
        let(:parsed) { described_class.new(url) }

        context 'when url is a sub-comment', focus: true do
          let(:url) { 'https://www.reddit.com/r/funny/comments/469n5e/how_my_brain_works/d03k8b8' }

          it 'determines subreddit from url' do
            expect(parsed.subreddit).to eq('/r/funny')
          end

          it 'determines original post id' do
            expect(parsed.original_post_id).to eq('469n5e')
          end

          it 'determines original post stub' do
            expect(parsed.original_post_stub).to eq('how_my_brain_works')
          end

          it 'determines comment id' do
            expect(parsed.comment_id).to eq('d03k8b8')
          end

          context '.original_post?' do
            it 'returns false' do
              expect(parsed.original_post?).to eq(false)
            end
          end
        end

        context 'when url is an original post', focus: true do
          let(:url) { 'https://www.reddit.com/r/funny/comments/469n5e/how_my_brain_works/' }

          it 'determines subreddit from url' do
            expect(parsed.subreddit).to eq('/r/funny')
          end

          it 'determines original post id' do
            expect(parsed.original_post_id).to eq('469n5e')
          end

          it 'determines original post stub' do
            expect(parsed.original_post_stub).to eq('how_my_brain_works')
          end

          it 'determines comment id as nil' do
            expect(parsed.comment_id).to be_nil
          end

          context '.original_post?' do
            it 'returns true' do
              expect(parsed.original_post?).to eq(true)
            end
          end
        end
      end
    end
  end
end
