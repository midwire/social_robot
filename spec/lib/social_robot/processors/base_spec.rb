require 'spec_helper'

module SocialRobot
  module Processors
    RSpec.describe Base, type: :processor do
      let(:user) { double('User') }

      context '#initialize' do
        it 'requires a user' do
          expect do
            described_class.new
          end.to raise_error(RuntimeError, /provide a user/)
        end

        it 'sets @user' do
          base = described_class.new(user: user)
          expect(base.user).to eq(user)
        end

        it 'sets a default @agent' do
          base = described_class.new(user: user)
          expect(base.agent).to be_an(Agent)
        end

        it 'sets @agent' do
          agent = Agent.new
          base = described_class.new(user: user, agent: agent)
          expect(base.agent).to eq(agent)
        end
      end
    end
  end
end
