require 'spec_helper'

module SocialRobot
  RSpec.describe User, type: :model do
    let(:username) { 'MyUserName' }

    let(:args) do
      {
        username: username,
        password: 'a-password',
        email: 'bogus@example.com',
        proxy_host: '123.123.123.123',
        proxy_port: '8080',
        proxy_user: 'bogusun',
        proxy_pass: 'boguspw'
      }
    end

    let(:user) { described_class.new(args) }

    context '#new' do
      it 'instantiates a User instance' do
        expect(user).to be_a(described_class)
      end

      it 'fails if no username' do
        expect do
          described_class.new(args.except(:username))
        end.to raise_error(RuntimeError, /is required/)
      end

      it 'fails if no pasword' do
        expect do
          described_class.new(args.except(:password))
        end.to raise_error(RuntimeError, /is required/)
      end

      [:username, :password, :email, :proxy_host, :proxy_port,
        :proxy_user, :proxy_pass].each do |attribute|
        it "sets @#{attribute}" do
          expect(user.send(attribute)).to_not be_nil
        end
      end
    end

    context '#deserialize' do
      it 'loads the user instance from disk' do
        expect(User.deserialize(username)).to be_a(described_class)
      end
    end

    context '.cookie_file' do
      it 'returns the cookie file for this user' do
        expect(user.cookie_file).to match(/#{username}.yml$/)
      end
    end

    context '.serialize' do
      it 'stores the user instance' do
        user.serialize
        expect(File.exist?(described_class.data_file(username))).to be(true)
      end
    end
  end
end
